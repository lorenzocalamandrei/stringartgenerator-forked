# StringArtGenerator
Web based string art generator

This project is forked from: https://github.com/halfmonty/StringArtGenerator

__To use ths generator, navigate to: https://lorenzocalamandrei.gitlab.io/stringartgenerator-forked/__

## Preview of how it works:

![](test2.gif)
